import SwClockSimple from './SwClockSimple.vue'

if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.component('SwClockSimple', SwClockSimple)
}

export default SwClockSimple
